package me.mathiaseklund.simplemanhunt;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class RunnerCommand implements CommandExecutor {

	Main main = Main.getMain();
	Players players = main.getPlayers();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				if (sender.hasPermission("manhunt.admin")) {
					if (args.length == 0) {
						// TODO send list of commands.
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"&eUsage:&7 /runner <playerName> &8-&f Toggles a players Speedrunner status."));
					} else {
						String targetName = args[0];
						Player target = Bukkit.getPlayer(targetName);
						if (target != null) {
							players.toggleRunner(target);
							if (players.isRunner(target)) {
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&a" + target.getName() + " is now a Speedrunner!"));
							} else {
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&c" + target.getName() + " is no longer a Speedrunner!"));
							}
						} else {
							sender.sendMessage(
									ChatColor.translateAlternateColorCodes('&', "&4ERROR:&7 Target player not found."));
						}
					}
				}
			}
		});
		return true;
	}

}

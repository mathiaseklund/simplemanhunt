package me.mathiaseklund.simplemanhunt;

import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;

public class Players {

	Main main;
	Config cfg;

	public Players() {
		main = Main.getMain();
		cfg = main.getCfg();

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				updateHunterTargets();
			}
		});
	}

	public boolean isRunner(Player player) {
		return cfg.runners.contains(player.getUniqueId().toString());
	}

	public boolean isHunter(Player player) {
		return cfg.hunters.contains(player.getUniqueId().toString());
	}

	public void toggleHunter(Player player) {
		if (cfg.runners.contains(player.getUniqueId().toString())) {
			cfg.runners.remove(player.getUniqueId().toString());
			cfg.save();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou are no longer a Speedrunner!"));
		}

		if (cfg.hunters.contains(player.getUniqueId().toString())) {
			cfg.hunters.remove(player.getUniqueId().toString());
			cfg.save();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou are no longer a Hunter!"));
			unhide(player);
			// if (player.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
			// Bukkit.getScheduler().runTask(main, new Runnable() {
			// public void run() {
			// player.removePotionEffect(PotionEffectType.INVISIBILITY);
			// }
			// });
			// }
			if (cfg.giveCompass) {
				for (int i = 0; i < player.getInventory().getSize(); i++) {
					ItemStack is = player.getInventory().getItem(i);
					if (is != null) {
						if (is.getType() == Material.COMPASS) {
							player.getInventory().setItem(i, null);
						}
					}
				}
			}
		} else {
			cfg.hunters.add(player.getUniqueId().toString());
			cfg.save();
			if (cfg.invisibleHunters) {
				hide(player);
				// Bukkit.getScheduler().runTask(main, new Runnable() {
				// public void run() {
				// player.addPotionEffect(
				// new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE,
				// Integer.MAX_VALUE));
				// }
				// });

			}
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou are now a Speedrunner!"));
			if (cfg.giveCompass) {
				ItemStack is = new ItemStack(Material.COMPASS);
				player.getInventory().addItem(is);
			}
		}
	}

	public void toggleRunner(Player player) {
		if (cfg.hunters.contains(player.getUniqueId().toString())) {
			cfg.hunters.remove(player.getUniqueId().toString());
			cfg.save();
			unhide(player);
			// if (player.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
			// Bukkit.getScheduler().runTask(main, new Runnable() {
			// public void run() {
			// player.removePotionEffect(PotionEffectType.INVISIBILITY);
			// }
			// });
			// }
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou are no longer a Hunter!"));
			if (cfg.giveCompass) {
				for (int i = 0; i < player.getInventory().getSize(); i++) {
					ItemStack is = player.getInventory().getItem(i);
					if (is != null) {
						if (is.getType() == Material.COMPASS) {
							player.getInventory().setItem(i, null);
						}
					}
				}
			}
		}

		if (cfg.runners.contains(player.getUniqueId().toString())) {
			cfg.runners.remove(player.getUniqueId().toString());
			cfg.save();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou are no longer a Speedrunner!"));
		} else {
			cfg.runners.add(player.getUniqueId().toString());
			cfg.save();
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou are now a Hunter!"));
		}
	}

	public void updateHunterTargets() {
		List<Player> runners = Lists.newArrayList();
		for (String s : cfg.runners) {
			Player p = Bukkit.getPlayer(UUID.fromString(s));
			if (!runners.contains(p)) {
				runners.add(p);
			}
		}
		for (String s : cfg.hunters) {
			Player hunter = Bukkit.getPlayer(UUID.fromString(s));
			if (hunter != null) {
				Location hunterLocation = hunter.getLocation();
				Player target = null;
				double dist = 0;
				for (Player p : runners) {
					Location loc = p.getLocation();
					double distance = hunterLocation.distance(loc);
					if (target != null) {
						if (dist > distance) {
							target = p;
							dist = distance;
						}
					} else {
						target = p;
						dist = distance;
					}
				}

				if (target != null) {
					hunter.setCompassTarget(target.getLocation());
				}
			}
		}

		Bukkit.getScheduler().runTaskLaterAsynchronously(main, new Runnable() {
			public void run() {
				updateHunterTargets();
			}
		}, 20);
	}

	public void hide(Player player) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (isRunner(p)) {
				p.hidePlayer(main, player);
			} else {
				p.showPlayer(main, player);
			}
		}
	}

	public void unhide(Player player) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.showPlayer(main, player);
		}
	}

}

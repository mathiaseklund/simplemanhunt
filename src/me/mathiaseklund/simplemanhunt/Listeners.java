package me.mathiaseklund.simplemanhunt;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class Listeners implements Listener {

	Main main = Main.getMain();
	Config cfg = main.getCfg();

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		if (main.getPlayers().isHunter(player)) {
			if (cfg.invisibleHunters) {
				// Bukkit.getScheduler().runTaskLater(main, new Runnable() {
				// public void run() {
				// player.addPotionEffect(
				// new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE,
				// Integer.MAX_VALUE));
				// }
				// }, 1);
				main.getPlayers().hide(player);
			}
		}

	}
}

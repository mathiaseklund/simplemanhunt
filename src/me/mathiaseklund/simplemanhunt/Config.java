package me.mathiaseklund.simplemanhunt;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {

	Main main;
	File f;
	FileConfiguration fc;

	List<String> hunters, runners;
	boolean storeData, giveCompass, invisibleHunters;

	public Config() {
		main = Main.getMain();

		f = new File(main.getDataFolder(), "config.yml");
		if (!f.exists()) {
			main.saveResource("config.yml", true);
		}
		fc = YamlConfiguration.loadConfiguration(f);

		load();
	}

	void load() {
		storeData = fc.getBoolean("store-data", true);
		hunters = fc.getStringList("hunters");
		runners = fc.getStringList("runners");
		giveCompass = fc.getBoolean("give-compass", true);
		invisibleHunters = fc.getBoolean("invisible-hunters", false);
	}

	public void save() {
		if (storeData) {
			try {
				fc.save(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

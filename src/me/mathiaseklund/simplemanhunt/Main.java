package me.mathiaseklund.simplemanhunt;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	static Main main;
	Config cfg;
	Players players;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;

		cfg = new Config();
		players = new Players();

		loadListeners();
		loadCommands();
	}

	void loadListeners() {
		Bukkit.getPluginManager().registerEvents(new Listeners(), this);
	}

	void loadCommands() {
		getCommand("hunter").setExecutor(new HunterCommand());
		getCommand("runner").setExecutor(new RunnerCommand());
	}

	public Config getCfg() {
		return cfg;
	}

	public Players getPlayers() {
		return players;
	}
}
